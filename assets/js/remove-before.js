/*Remove the before pseudo element from <div class="post__iframe"></div> elements enclosing a hidden iframe with id="hidden_iframe" */
/*It applies to hidden iframes associated with google forms*/

var hidden_iframe = document.getElementById("hidden_iframe");
//console.log(hidden_iframe);
if(hidden_iframe){
		var div_post_iframe = hidden_iframe.closest("div.post__iframe");
		console.log(div_post_iframe);
		if(div_post_iframe){
			div_post_iframe.classList.add("no-before");
		}
}